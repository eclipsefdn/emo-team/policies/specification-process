# Contributing to Eclipse Vhant

Thanks for your interest in this project.

## Project description

The Eclipse Foundation Development Process (EDP) is the foundational goverance
document for Eclipse projects and committers. It describes the manner in which
we do open source software. The EDP does not prescribe any particular
development methodology; it is more concerned with the larger-scale aspects of
open source project lifecycle, including such things as reviews, processes for
running votes and elections, bringing new committers onto a project, etc. The
Eclipse Foundation Specification Process (EFSP) extends the EDP to describe the
process by which specifiations are created in open source. The Eclipse Vhant
project maintains the Eclipse Foundation Development Process, the Eclipse
Foundation Specification Process, the Standard Top-Level Project Charter, and
related resources.

* https://projects.eclipse.org/projects/technology.vhant

## Terms of Use

This repository is subject to the Terms of Use of the Eclipse Foundation

* https://www.eclipse.org/legal/termsofuse.php

## Developer resources

Information regarding source code management, builds, coding standards, and
more.

* https://projects.eclipse.org/projects/technology.vhant/developer

The project maintains the following source code repositories

* https://gitlab.eclipse.org/eclipse/vhant/development-process.git
* https://gitlab.eclipse.org/eclipse/vhant/specification-process.git
* https://gitlab.eclipse.org/eclipse/vhant/vhant.git

## Eclipse Development Process

This Eclipse Foundation open project is governed by the Eclipse Foundation
Development Process and operates under the terms of the Eclipse IP Policy.

* https://eclipse.org/projects/dev_process
* https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf

## Eclipse Contributor Agreement

In order to be able to contribute to Eclipse Foundation projects you must
electronically sign the Eclipse Contributor Agreement (ECA).

* https://www.eclipse.org/legal/ECA.php

The ECA provides the Eclipse Foundation with a permanent record that you agree
that each of your contributions will comply with the commitments documented in
the Developer Certificate of Origin (DCO). Having an ECA on file associated with
the email address matching the "Author" field of your contribution's Git commits
fulfils the DCO's requirement that you sign-off on your contributions.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* https://dev.eclipse.org/mailman/listinfo/vhant-dev
